﻿//-----------------------------------------------------------------------
// <copyright file="TwitterSearchResultCollection.cs" company="Patrick 'Ricky' Smith">
//  This file is part of the Twitterizer library (http://www.twitterizer.net/)
// 
//  Copyright (c) 2010, Patrick "Ricky" Smith (ricky@digitally-born.com)
//  All rights reserved.
//  
//  Redistribution and use in source and binary forms, with or without modification, are 
//  permitted provided that the following conditions are met:
// 
//  - Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  - Redistributions in binary form must reproduce the above copyright notice, this list 
//    of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  - Neither the name of the Twitterizer nor the names of its contributors may be 
//    used to endorse or promote products derived from this software without specific 
//    prior written permission.
// 
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
//  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
//  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
//  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
//  POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// <author>Ricky Smith</author>
// <summary>The twitter search result collection class</summary>
//-----------------------------------------------------------------------

using Twitterizer.Core;

namespace Twitterizer
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// The Twitter Search Result Collection class
    /// </summary>
#if !SILVERLIGHT
    [Serializable]
#endif
    public class TwitterSearchResultCollection : Core.TwitterCollection<TwitterSearchResult>, ITwitterObject
    {
        /// <summary>
        /// Gets or sets the completed_in.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public double CompletedIn { get; internal set; }

        /// <summary>
        /// Gets or sets the max_id.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public long MaxId { get; internal set; }

        /// <summary>
        /// Gets or sets the max_id as a string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public string MaxIdStr { get; internal set; }

        /// <summary>
        /// Gets or sets the next_page.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        //public string NextPage { get; internal set; }

        /// <summary>
        /// Gets or sets the query.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public string Query { get; internal set; }

        /// <summary>
        /// Gets or sets the refresh URL.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public string RefreshUrl { get; internal set; }

        /// <summary>
        /// Deserializes the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal static TwitterSearchResultCollection Deserialize(JObject value)
        {
            if (value == null || value["statuses"] == null)
                return null;

            TwitterSearchResultCollection result = JsonConvert.DeserializeObject<TwitterSearchResultCollection>(value["statuses"].ToString());
            result.CompletedIn = value.SelectToken("search_metadata.completed_in").Value<double>();
            result.MaxId = value.SelectToken("search_metadata.max_id").Value<long>();
            result.MaxIdStr = value.SelectToken("search_metadata.max_id_str").Value<string>();
            result.Query = value.SelectToken("search_metadata.query").Value<string>();
            result.RefreshUrl = value.SelectToken("search_metadata.refresh_url").Value<string>();
            int i;
            for (i = 0; i <= result.Count -1 ; i++)
            {
                switch (i)
                {
                    #region 振り分け
                    case 0:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[0].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[0].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[0].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[0].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[0].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[0].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[0].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[0].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[0].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[0].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[0].user.location").Value<string>();
                        break;
                    case 1:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[1].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[1].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[1].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[1].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[1].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[1].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[1].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[1].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[1].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[1].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[1].user.location").Value<string>();
                        break;
					case 2:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[2].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[2].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[2].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[2].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[2].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[2].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[2].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[2].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[2].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[2].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[2].user.location").Value<string>();
                        break;
case 3:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[3].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[3].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[3].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[3].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[3].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[3].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[3].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[3].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[3].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[3].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[3].user.location").Value<string>();
                        break;
case 4:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[4].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[4].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[4].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[4].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[4].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[4].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[4].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[4].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[4].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[4].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[4].user.location").Value<string>();
                        break;
case 5:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[5].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[5].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[5].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[5].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[5].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[5].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[5].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[5].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[5].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[5].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[5].user.location").Value<string>();
                        break;
case 6:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[6].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[6].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[6].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[6].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[6].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[6].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[6].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[6].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[6].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[6].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[6].user.location").Value<string>();
                        break;
case 7:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[7].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[7].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[7].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[7].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[7].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[7].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[7].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[7].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[7].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[7].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[7].user.location").Value<string>();
                        break;
case 8:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[8].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[8].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[8].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[8].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[8].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[8].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[8].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[8].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[8].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[8].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[8].user.location").Value<string>();
                        break;
case 9:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[9].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[9].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[9].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[9].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[9].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[9].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[9].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[9].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[9].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[9].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[9].user.location").Value<string>();
                        break;
case 10:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[10].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[10].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[10].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[10].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[10].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[10].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[10].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[10].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[10].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[10].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[10].user.location").Value<string>();
                        break;
case 11:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[11].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[11].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[11].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[11].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[11].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[11].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[11].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[11].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[11].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[11].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[11].user.location").Value<string>();
                        break;
case 12:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[12].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[12].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[12].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[12].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[12].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[12].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[12].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[12].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[12].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[12].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[12].user.location").Value<string>();
                        break;
case 13:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[13].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[13].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[13].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[13].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[13].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[13].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[13].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[13].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[13].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[13].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[13].user.location").Value<string>();
                        break;
case 14:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[14].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[14].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[14].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[14].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[14].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[14].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[14].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[14].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[14].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[14].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[14].user.location").Value<string>();
                        break;
case 15:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[15].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[15].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[15].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[15].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[15].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[15].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[15].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[15].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[15].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[15].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[15].user.location").Value<string>();
                        break;
case 16:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[16].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[16].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[16].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[16].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[16].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[16].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[16].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[16].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[16].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[16].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[16].user.location").Value<string>();
                        break;
case 17:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[17].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[17].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[17].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[17].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[17].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[17].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[17].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[17].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[17].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[17].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[17].user.location").Value<string>();
                        break;
case 18:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[18].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[18].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[18].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[18].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[18].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[18].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[18].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[18].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[18].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[18].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[18].user.location").Value<string>();
                        break;
case 19:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[19].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[19].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[19].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[19].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[19].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[19].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[19].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[19].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[19].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[19].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[19].user.location").Value<string>();
                        break;
case 20:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[20].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[20].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[20].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[20].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[20].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[20].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[20].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[20].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[20].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[20].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[20].user.location").Value<string>();
                        break;
case 21:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[21].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[21].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[21].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[21].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[21].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[21].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[21].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[21].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[21].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[21].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[21].user.location").Value<string>();
                        break;
case 22:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[22].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[22].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[22].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[22].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[22].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[22].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[22].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[22].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[22].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[22].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[22].user.location").Value<string>();
                        break;
case 23:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[23].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[23].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[23].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[23].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[23].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[23].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[23].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[23].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[23].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[23].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[23].user.location").Value<string>();
                        break;
case 24:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[24].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[24].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[24].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[24].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[24].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[24].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[24].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[24].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[24].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[24].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[24].user.location").Value<string>();
                        break;
case 25:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[25].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[25].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[25].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[25].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[25].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[25].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[25].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[25].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[25].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[25].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[25].user.location").Value<string>();
                        break;
case 26:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[26].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[26].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[26].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[26].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[26].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[26].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[26].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[26].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[26].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[26].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[26].user.location").Value<string>();
                        break;
case 27:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[27].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[27].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[27].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[27].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[27].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[27].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[27].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[27].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[27].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[27].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[27].user.location").Value<string>();
                        break;
case 28:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[28].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[28].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[28].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[28].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[28].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[28].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[28].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[28].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[28].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[28].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[28].user.location").Value<string>();
                        break;
case 29:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[29].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[29].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[29].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[29].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[29].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[29].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[29].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[29].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[29].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[29].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[29].user.location").Value<string>();
                        break;
case 30:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[30].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[30].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[30].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[30].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[30].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[30].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[30].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[30].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[30].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[30].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[30].user.location").Value<string>();
                        break;
case 31:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[31].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[31].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[31].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[31].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[31].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[31].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[31].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[31].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[31].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[31].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[31].user.location").Value<string>();
                        break;
case 32:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[32].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[32].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[32].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[32].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[32].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[32].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[32].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[32].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[32].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[32].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[32].user.location").Value<string>();
                        break;
case 33:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[33].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[33].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[33].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[33].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[33].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[33].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[33].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[33].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[33].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[33].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[33].user.location").Value<string>();
                        break;
case 34:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[34].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[34].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[34].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[34].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[34].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[34].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[34].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[34].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[34].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[34].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[34].user.location").Value<string>();
                        break;
case 35:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[35].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[35].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[35].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[35].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[35].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[35].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[35].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[35].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[35].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[35].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[35].user.location").Value<string>();
                        break;
case 36:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[36].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[36].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[36].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[36].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[36].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[36].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[36].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[36].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[36].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[36].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[36].user.location").Value<string>();
                        break;
case 37:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[37].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[37].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[37].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[37].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[37].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[37].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[37].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[37].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[37].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[37].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[37].user.location").Value<string>();
                        break;
case 38:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[38].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[38].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[38].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[38].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[38].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[38].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[38].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[38].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[38].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[38].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[38].user.location").Value<string>();
                        break;
case 39:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[39].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[39].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[39].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[39].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[39].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[39].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[39].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[39].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[39].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[39].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[39].user.location").Value<string>();
                        break;
case 40:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[40].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[40].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[40].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[40].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[40].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[40].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[40].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[40].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[40].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[40].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[40].user.location").Value<string>();
                        break;
case 41:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[41].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[41].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[41].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[41].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[41].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[41].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[41].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[41].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[41].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[41].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[41].user.location").Value<string>();
                        break;
case 42:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[42].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[42].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[42].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[42].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[42].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[42].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[42].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[42].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[42].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[42].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[42].user.location").Value<string>();
                        break;
case 43:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[43].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[43].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[43].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[43].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[43].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[43].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[43].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[43].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[43].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[43].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[43].user.location").Value<string>();
                        break;
case 44:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[44].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[44].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[44].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[44].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[44].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[44].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[44].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[44].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[44].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[44].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[44].user.location").Value<string>();
                        break;
case 45:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[45].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[45].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[45].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[45].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[45].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[45].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[45].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[45].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[45].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[45].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[45].user.location").Value<string>();
                        break;
case 46:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[46].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[46].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[46].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[46].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[46].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[46].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[46].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[46].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[46].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[46].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[46].user.location").Value<string>();
                        break;
case 47:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[47].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[47].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[47].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[47].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[47].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[47].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[47].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[47].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[47].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[47].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[47].user.location").Value<string>();
                        break;
case 48:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[48].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[48].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[48].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[48].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[48].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[48].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[48].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[48].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[48].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[48].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[48].user.location").Value<string>();
                        break;
case 49:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[49].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[49].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[49].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[49].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[49].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[49].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[49].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[49].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[49].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[49].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[49].user.location").Value<string>();
                        break;
case 50:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[50].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[50].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[50].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[50].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[50].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[50].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[50].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[50].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[50].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[50].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[50].user.location").Value<string>();
                        break;
case 51:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[51].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[51].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[51].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[51].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[51].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[51].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[51].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[51].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[51].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[51].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[51].user.location").Value<string>();
                        break;
case 52:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[52].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[52].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[52].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[52].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[52].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[52].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[52].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[52].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[52].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[52].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[52].user.location").Value<string>();
                        break;
case 53:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[53].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[53].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[53].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[53].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[53].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[53].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[53].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[53].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[53].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[53].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[53].user.location").Value<string>();
                        break;
case 54:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[54].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[54].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[54].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[54].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[54].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[54].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[54].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[54].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[54].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[54].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[54].user.location").Value<string>();
                        break;
case 55:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[55].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[55].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[55].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[55].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[55].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[55].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[55].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[55].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[55].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[55].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[55].user.location").Value<string>();
                        break;
case 56:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[56].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[56].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[56].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[56].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[56].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[56].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[56].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[56].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[56].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[56].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[56].user.location").Value<string>();
                        break;
case 57:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[57].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[57].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[57].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[57].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[57].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[57].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[57].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[57].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[57].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[57].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[57].user.location").Value<string>();
                        break;
case 58:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[58].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[58].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[58].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[58].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[58].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[58].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[58].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[58].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[58].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[58].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[58].user.location").Value<string>();
                        break;
case 59:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[59].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[59].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[59].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[59].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[59].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[59].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[59].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[59].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[59].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[59].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[59].user.location").Value<string>();
                        break;
case 60:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[60].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[60].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[60].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[60].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[60].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[60].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[60].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[60].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[60].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[60].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[60].user.location").Value<string>();
                        break;
case 61:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[61].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[61].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[61].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[61].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[61].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[61].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[61].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[61].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[61].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[61].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[61].user.location").Value<string>();
                        break;
case 62:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[62].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[62].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[62].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[62].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[62].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[62].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[62].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[62].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[62].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[62].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[62].user.location").Value<string>();
                        break;
case 63:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[63].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[63].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[63].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[63].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[63].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[63].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[63].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[63].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[63].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[63].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[63].user.location").Value<string>();
                        break;
case 64:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[64].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[64].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[64].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[64].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[64].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[64].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[64].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[64].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[64].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[64].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[64].user.location").Value<string>();
                        break;
case 65:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[65].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[65].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[65].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[65].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[65].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[65].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[65].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[65].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[65].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[65].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[65].user.location").Value<string>();
                        break;
case 66:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[66].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[66].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[66].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[66].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[66].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[66].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[66].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[66].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[66].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[66].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[66].user.location").Value<string>();
                        break;
case 67:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[67].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[67].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[67].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[67].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[67].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[67].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[67].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[67].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[67].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[67].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[67].user.location").Value<string>();
                        break;
case 68:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[68].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[68].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[68].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[68].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[68].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[68].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[68].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[68].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[68].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[68].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[68].user.location").Value<string>();
                        break;
case 69:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[69].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[69].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[69].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[69].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[69].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[69].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[69].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[69].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[69].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[69].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[69].user.location").Value<string>();
                        break;
case 70:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[70].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[70].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[70].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[70].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[70].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[70].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[70].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[70].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[70].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[70].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[70].user.location").Value<string>();
                        break;
case 71:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[71].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[71].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[71].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[71].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[71].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[71].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[71].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[71].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[71].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[71].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[71].user.location").Value<string>();
                        break;
case 72:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[72].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[72].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[72].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[72].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[72].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[72].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[72].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[72].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[72].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[72].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[72].user.location").Value<string>();
                        break;
case 73:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[73].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[73].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[73].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[73].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[73].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[73].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[73].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[73].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[73].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[73].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[73].user.location").Value<string>();
                        break;
case 74:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[74].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[74].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[74].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[74].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[74].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[74].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[74].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[74].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[74].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[74].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[74].user.location").Value<string>();
                        break;
case 75:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[75].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[75].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[75].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[75].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[75].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[75].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[75].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[75].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[75].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[75].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[75].user.location").Value<string>();
                        break;
case 76:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[76].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[76].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[76].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[76].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[76].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[76].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[76].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[76].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[76].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[76].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[76].user.location").Value<string>();
                        break;
case 77:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[77].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[77].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[77].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[77].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[77].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[77].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[77].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[77].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[77].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[77].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[77].user.location").Value<string>();
                        break;
case 78:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[78].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[78].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[78].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[78].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[78].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[78].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[78].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[78].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[78].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[78].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[78].user.location").Value<string>();
                        break;
case 79:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[79].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[79].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[79].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[79].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[79].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[79].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[79].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[79].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[79].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[79].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[79].user.location").Value<string>();
                        break;
case 80:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[80].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[80].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[80].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[80].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[80].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[80].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[80].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[80].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[80].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[80].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[80].user.location").Value<string>();
                        break;
case 81:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[81].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[81].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[81].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[81].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[81].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[81].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[81].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[81].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[81].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[81].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[81].user.location").Value<string>();
                        break;
case 82:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[82].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[82].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[82].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[82].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[82].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[82].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[82].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[82].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[82].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[82].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[82].user.location").Value<string>();
                        break;
case 83:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[83].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[83].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[83].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[83].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[83].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[83].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[83].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[83].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[83].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[83].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[83].user.location").Value<string>();
                        break;
case 84:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[84].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[84].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[84].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[84].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[84].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[84].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[84].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[84].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[84].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[84].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[84].user.location").Value<string>();
                        break;
case 85:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[85].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[85].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[85].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[85].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[85].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[85].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[85].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[85].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[85].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[85].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[85].user.location").Value<string>();
                        break;
case 86:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[86].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[86].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[86].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[86].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[86].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[86].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[86].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[86].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[86].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[86].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[86].user.location").Value<string>();
                        break;
case 87:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[87].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[87].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[87].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[87].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[87].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[87].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[87].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[87].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[87].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[87].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[87].user.location").Value<string>();
                        break;
case 88:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[88].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[88].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[88].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[88].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[88].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[88].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[88].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[88].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[88].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[88].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[88].user.location").Value<string>();
                        break;
case 89:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[89].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[89].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[89].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[89].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[89].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[89].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[89].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[89].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[89].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[89].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[89].user.location").Value<string>();
                        break;
case 90:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[90].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[90].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[90].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[90].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[90].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[90].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[90].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[90].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[90].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[90].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[90].user.location").Value<string>();
                        break;
case 91:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[91].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[91].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[91].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[91].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[91].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[91].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[91].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[91].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[91].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[91].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[91].user.location").Value<string>();
                        break;
case 92:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[92].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[92].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[92].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[92].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[92].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[92].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[92].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[92].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[92].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[92].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[92].user.location").Value<string>();
                        break;
case 93:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[93].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[93].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[93].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[93].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[93].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[93].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[93].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[93].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[93].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[93].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[93].user.location").Value<string>();
                        break;
case 94:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[94].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[94].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[94].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[94].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[94].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[94].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[94].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[94].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[94].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[94].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[94].user.location").Value<string>();
                        break;
case 95:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[95].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[95].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[95].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[95].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[95].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[95].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[95].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[95].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[95].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[95].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[95].user.location").Value<string>();
                        break;
case 96:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[96].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[96].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[96].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[96].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[96].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[96].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[96].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[96].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[96].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[96].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[96].user.location").Value<string>();
                        break;
case 97:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[97].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[97].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[97].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[97].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[97].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[97].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[97].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[97].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[97].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[97].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[97].user.location").Value<string>();
                        break;
case 98:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[98].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[98].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[98].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[98].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[98].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[98].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[98].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[98].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[98].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[98].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[98].user.location").Value<string>();
                        break;
case 99:
                        result[i].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[99].created_at").Value<string>()));
                        result[i].ProfileImageLocation = value.SelectToken("statuses[99].user.profile_image_url").Value<string>();
                        result[i].FromUserDisplayName = value.SelectToken("statuses[99].user.name").Value<string>();
                        result[i].FromUserScreenName = value.SelectToken("statuses[99].user.screen_name").Value<string>();
                        result[i].FromUserId = value.SelectToken("statuses[99].user.id_str").Value<Decimal>();
                        result[i].Text = value.SelectToken("statuses[99].text").Value<string>();
                        result[i].Id = value.SelectToken("statuses[99].id_str").Value<decimal>();
                        result[i].Source = value.SelectToken("statuses[99].source").Value<string>();
                        result[i].Language = value.SelectToken("statuses[99].metadata.iso_language_code").Value<string>();
                        result[i].Geo = value.SelectToken("statuses[99].geo").Value<Twitterizer.TwitterGeo>();
                        result[i].Location = value.SelectToken("statuses[99].user.location").Value<string>();
                        break;
case 100:
                        result[100].CreatedDate = new TwitterstringDateParse().Parse((value.SelectToken("statuses[100].created_at").Value<string>()));
                        result[100].ProfileImageLocation = value.SelectToken("statuses[100].user.profile_image_url").Value<string>();
                        result[100].FromUserDisplayName = value.SelectToken("statuses[100].user.name").Value<string>();
                        result[100].FromUserScreenName = value.SelectToken("statuses[100].user.screen_name").Value<string>();
                        result[100].FromUserId = value.SelectToken("statuses[100].user.id_str").Value<Decimal>();
                        result[100].Text = value.SelectToken("statuses[100].text").Value<string>();
                        result[100].Id = value.SelectToken("statuses[100].id_str").Value<decimal>();
                        result[100].Source = value.SelectToken("statuses[100].source").Value<string>();
                        result[100].Language = value.SelectToken("statuses[100].metadata.iso_language_code").Value<string>();
                        result[100].Geo = value.SelectToken("statuses[100].geo").Value<Twitterizer.TwitterGeo>();
                        result[100].Location = value.SelectToken("statuses[100].user.location").Value<string>();
                        break;
                }
# endregion
        
                //result[i].InReplyToStatusId = value.SelectToken("statuses[i].in_reply_to_status_id").Value<decimal>();
            }

            return result;
        }
       
    }
}